![Lasagne](media/logo.svg)

Lasagne is an attempt to create an amd64 operating system written in assembly, C and Rust.

## How to build

### Requirements

To build the kernel you need:
- GCC cross compiler - details below
- Rust - details below
- NASM
- grub-mkrescue (to build bootable .iso image)
- qemu-system-x86 and gdb for running and debugging
- xorriso to create the .iso image

#### GCC

GCC cross compiler is needed to compile the kernel.
It can be built using dockerfile in `toolchain` directory.
The container image has the toolchain installed to /opt/lasange-gcc and packed to `/toolchain.tar`
so it can be copied out of container if desired.

#### Rust

- install Rust nightly through rustup
- `cargo install cargo-xbuild`
- `rustup component add rust-src`

### Build and emulation

```sh
cd kernel
make gdb
```

## Screenshots
[You can see screenshots on a wiki page](https://github.com/szpght/lasagne/wiki/Screenshots).

## Credits

Logo created by [@logarytm](https://github.com/logarytm).
