#include <io/io.h>
#include <io/pit.h>
#include <io/tty.h>
#include <irq.h>
#include <mm/alloc.h>
#include <mm/frame.h>
#include <mm/memory_map.h>
#include <mm/pages.h>
#include <multiboot.h>
#include <printk.h>
#include <task.h>
#include <syscall.h>

void rtc_interrupt_handler();

__init void initialize(void *multiboot_information)
{
    initialize_serial_port();
    initialize_tty();
    initialize_irq();
    parse_multiboot(multiboot_information);
    initialize_frame_allocation();
    initialize_virtual_memory();
    initialize_kernel_heap();
    initialize_tasks();
    initialize_syscalls();
    initialize_pit();
    set_irq_handler(0x28, rtc_interrupt_handler, 0);
    initialize_rtc();
    printk("SYSTEM BOOT COMPLETE\n");

    while(1){}
}
