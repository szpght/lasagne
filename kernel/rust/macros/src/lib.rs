extern crate proc_macro;
use proc_macro::TokenStream;
use std::str::FromStr;
use core::iter::IntoIterator;
use proc_macro::TokenTree::Ident;
use std::iter::Iterator;
use core::iter::FromIterator;

fn prepend_code_to_item(item: TokenStream, code: &str) -> TokenStream {
    let mut new_item = TokenStream::from_str(&code).unwrap();
    new_item.extend(item);
    new_item
}

fn prepend_link_section(item: TokenStream, section: &str) -> TokenStream {
    let code = format!("#[link_section = \"{}\"]", section);
    prepend_code_to_item(item, &code)
}

#[proc_macro_attribute]
pub fn init_text(_attr: TokenStream, item: TokenStream) -> TokenStream {
    prepend_link_section(item, ".text.init")
}

#[proc_macro_attribute]
pub fn init_data(_attr: TokenStream, item: TokenStream) -> TokenStream {
    prepend_link_section(item, ".data.init")
}

#[proc_macro_attribute]
pub fn exported(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let mut glorious_iterator = item.into_iter();

    let tokens_before = glorious_iterator.by_ref().take_while(|token| match token {
        Ident(ident) => ident.to_string() != "fn",
        _ => true,
    });
    let mut new_item = TokenStream::from_iter(tokens_before);

    let code = "#[no_mangle] pub extern \"sysv64\" fn"; // restore fn swallowed by take_while
    let export_tokens = TokenStream::from_str(code).unwrap();
    new_item.extend(export_tokens);
    new_item.extend(glorious_iterator);
    new_item
}
