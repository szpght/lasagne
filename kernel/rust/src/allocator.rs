use alloc::alloc::{GlobalAlloc, Layout};

pub struct Dummy;

extern "C" {
    fn kalloc(size: usize) -> *mut u8;
    fn kzalloc(size: usize) -> *mut u8;
    fn krealloc(ptr: *mut u8, size: usize) -> *mut u8;
    fn kfree(ptr: *mut u8);
}

// let's use the fact that both value of align
// and allocations from buddy allocator are power of two-aligned
// so the allocation is always aligned if it's size is
// equal of greater to align
fn get_size(layout: Layout) -> usize {
    let align = layout.align();
    let size = layout.size();
    if align > size {
        align
    } else {
        size
    }
}

unsafe impl GlobalAlloc for Dummy {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        kalloc(get_size(layout))
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        kfree(ptr);
    }

    unsafe fn alloc_zeroed(&self, layout: Layout) -> *mut u8 {
        kzalloc(get_size(layout))
    }

    unsafe fn realloc(&self, ptr: *mut u8, layout: Layout, new_size: usize) -> *mut u8 {
        let align = layout.align();
        if new_size > align {
            krealloc(ptr, new_size)
        } else {
            krealloc(ptr, align)
        }
    }
}
