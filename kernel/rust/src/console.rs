use core::fmt;

extern "C" {
    fn backend(character: u8);
}

fn print_character(character: u8) {
    unsafe {
        backend(character);
    }
}

fn write_string(s: &str) {
    for byte in s.bytes() {
        match byte {
            0x20..=0x7e | b'\n' => print_character(byte),
            _ => print_character(0xfe),
        }
    }
}

pub struct Writer {}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        write_string(s);
        Ok(())
    }
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::console::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    Writer {}.write_fmt(args).unwrap();
}
