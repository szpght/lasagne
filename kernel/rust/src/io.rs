//! Dumping ground for functions communicating with hardware
//! that are implemented in C or assembly.

extern "C" {
    fn inb(port: u16) -> u8;
    fn outb(port: u16, data: u8);
    fn disable_irq();
    fn enable_irq();
}

pub fn io_input(port: u16) -> u8 {
    unsafe { inb(port) }
}

pub fn io_output(port: u16, data: u8) {
    unsafe { outb(port, data) }
}

pub fn disable_interrupts() {
    unsafe { disable_irq() }
}

pub fn enable_interrupts() {
    unsafe { enable_irq() }
}
