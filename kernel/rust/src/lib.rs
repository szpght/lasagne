#![no_std]
#![feature(alloc_error_handler)]
#![feature(panic_info_message)]
#![feature(const_fn)]

use core::fmt::write;
use core::panic::PanicInfo;

extern crate alloc;
extern crate macros;

pub mod allocator;
pub mod console;
pub mod io;
pub mod pic;
pub mod serial;
pub mod time;

extern "C" {
    fn halt() -> !;
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    print!("Kernel panic ");

    if let Some(location) = info.location() {
        println!("in file '{}' at line {}", location.file(), location.line(),);
    } else {
        println!();
    }

    if let Some(s) = info.message() {
        write(&mut console::Writer {}, *s).unwrap();
        println!();
    }

    unsafe { halt() }
}

#[global_allocator]
static ALLOCATOR: allocator::Dummy = allocator::Dummy;

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}
