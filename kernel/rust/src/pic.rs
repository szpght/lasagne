use crate::io::{io_input, io_output};
use crate::println;

const PIC1_CMD: u16 = 0x20;
const PIC1_DATA: u16 = 0x21;
const PIC2_CMD: u16 = 0xA0;
const PIC2_DATA: u16 = 0xA1;
const PIC_EOI: u8 = 0x20;
const PIC_INIT: u8 = 0x11;
const PIC_MASTER: u8 = 0x04;
const PIC_SLAVE: u8 = 0x02;
const PIC_8086: u8 = 0x01;

#[derive(Copy, Clone)]
pub struct Irq {
    pic: u8,
    number: u8,
}

static CASCADE_IRQ: Irq = Irq { pic: 1, number: 2 };
pub static RTC_IRQ: Irq = Irq { pic: 2, number: 0 };

impl Irq {
    pub fn enable(self) {
        let port = self.get_port();
        let mask = io_input(port);
        let mask = mask & !(1 << self.number);
        println!("setting mask {} to pic {}", mask, self.pic);
        io_output(port, mask);
        if self.pic == 2 {
            CASCADE_IRQ.enable();
        }
    }

    pub fn disable(self) {
        let port = self.get_port();
        let mask = io_input(port);
        let mask = mask | 1 << self.number;
        println!("setting mask {} to pic {} in disable", mask, self.pic);
        io_output(port, mask);
        if self.pic == 2 && mask == 0 {
            CASCADE_IRQ.disable();
        }
    }

    pub fn end(self) {
        io_output(PIC1_CMD, PIC_EOI);
        if self.pic == 2 {
            io_output(PIC2_CMD, PIC_EOI);
        }
    }

    fn get_port(self) -> u16 {
        if self.pic == 1 {
            PIC1_DATA
        } else {
            PIC2_DATA
        }
    }
}
