use crate::io;
use macros::{exported, init_text};

const COM1: u16 = 0x3F8;

#[exported]
#[init_text]
fn initialize_serial_port() {
    io::io_output(COM1 + 1, 0x00); // Disable all interrupts
    io::io_output(COM1 + 3, 0x80); // Enable DLAB (set baud rate divisor)
    io::io_output(COM1 + 0, 0x01); // Set divisor to 1 (lo byte)
    io::io_output(COM1 + 1, 0x00); // (hi byte)
    io::io_output(COM1 + 3, 0x03); // 8 bits, no parity, one stop bit
    io::io_output(COM1 + 2, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
    io::io_output(COM1 + 4, 0x0B); // IRQs enabled, RTS/DSR set
}

#[exported]
fn serial_write(data: u8) {
    while !write_ready() {
        // wait
    }

    io::io_output(COM1, data);
}

fn write_ready() -> bool {
    io::io_input(COM1 + 5) & 0x20 != 0
}
