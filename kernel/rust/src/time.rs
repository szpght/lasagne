use crate::io::{disable_interrupts, enable_interrupts, io_input, io_output};
use crate::pic::RTC_IRQ;
use crate::println;
use macros::{exported, init_text};

#[derive(PartialEq, Debug)]
pub struct DateTime {
    second: u8,
    minute: u8,
    hour: u8,
    day: u8,
    month: u8,
    year: i32,
}

#[init_text]
#[exported]
fn initialize_rtc() {
    disable_interrupts();
    let current_b_register = get_cmos_register(0x0B);
    let new_b_register = current_b_register | (1 << 4);
    set_cmos_register(0x0B, new_b_register);
    enable_interrupts();
    println!("Enabling RTC IRQ");
    RTC_IRQ.enable();
}

#[exported]
fn rtc_interrupt_handler() {
    let clock = Clock::initialize();
    let time = clock.get_time();
    println!("{:?}", time);
    get_cmos_register(0x0C);
    RTC_IRQ.end();
}

enum HourMode {
    Mode12H,
    Mode24H,
}

enum BinaryFormat {
    Binary,
    Bcd,
}

struct Clock {
    mode: HourMode,
    binary_format: BinaryFormat,
    year_offset: i32,
}

impl Clock {
    pub fn initialize() -> Clock {
        let status_register_b = get_cmos_register(0x0B);
        let mode24 = status_register_b & (1 << 1);
        let mode_binary = status_register_b & (1 << 2);
        let year_offset = 2000;
        println!(
            "Clock: century detection not implemented, assuming year offset {}",
            year_offset
        );
        Clock {
            mode: if mode24 != 0 {
                HourMode::Mode24H
            } else {
                HourMode::Mode12H
            },
            binary_format: if mode_binary != 0 {
                BinaryFormat::Binary
            } else {
                BinaryFormat::Bcd
            },
            year_offset: year_offset,
        }
    }

    pub fn get_time(&self) -> DateTime {
        let mut time1 = self.get_time_internal();
        let mut time2 = self.get_time_internal();

        while time1 != time2 {
            time1 = time2;
            time2 = self.get_time_internal();
        }

        time1
    }

    fn get_time_internal(&self) -> DateTime {
        while self.update_in_progress() {
            // wait
        }

        let second = get_cmos_register(0x00);
        let minute = get_cmos_register(0x02);
        let hour = get_cmos_register(0x04);
        let day = get_cmos_register(0x07);
        let month = get_cmos_register(0x08);
        let year = get_cmos_register(0x09);

        DateTime {
            second: self.format(second),
            minute: self.format(minute),
            hour: self.format_hour(hour),
            day: self.format(day),
            month: self.format(month),
            year: self.format(year) as i32 + self.year_offset,
        }
    }

    fn update_in_progress(&self) -> bool {
        let flag = get_cmos_register(0x0A) & (1 << 7);
        flag != 0
    }

    fn format(&self, value: u8) -> u8 {
        match self.binary_format {
            BinaryFormat::Binary => value,
            BinaryFormat::Bcd => (value & 0x0F) + (value >> 4) * 10,
        }
    }

    fn format_hour(&self, value: u8) -> u8 {
        let hour = self.format(value);
        match self.mode {
            HourMode::Mode24H => hour,
            HourMode::Mode12H => hour + 12 * (hour >> 7),
        }
    }
}

fn get_cmos_register(register: u8) -> u8 {
    let nmi_disabled = 0;
    io_output(0x70, nmi_disabled | register);
    io_input(0x71)
}

fn set_cmos_register(register: u8, value: u8) {
    let nmi_disabled = 0;
    io_output(0x70, nmi_disabled | register);
    io_output(0x71, value);
}
